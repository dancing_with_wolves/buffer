#include <stdio.h>
#include <stdlib.h>
#include "Buffer.h"

int main(){
	bufInit( "input.txt" );
	char a;
	int k;
	bscanf( "%d", &k );
	bscanf( "%l", &a );
	bscanf( "%c", &a );
	printf( "%d  %c\n", k, a );

	bseek( 0, 0 );
	bscanf( "%d", &k );
	printf( "%d\n", k );
	return 0;
}
