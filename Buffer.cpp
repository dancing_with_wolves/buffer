#include <stdio.h>
#include <stdlib.h>

char buffer[10000] = "";
int cur = 0;


int bufInit( const char* fname ){
	FILE *in = fopen( fname, "rb" );
	int ret = fread( buffer, sizeof(char), 10000, in );
	fclose( in );
	return ret;
}

void bseek( const int from, const int offset ){
	cur = from + offset;
}

int readInt(){
	char num[16] = "";
	int i = 0;
	while( buffer[cur] >= '0' && buffer[cur] <= '9' ){
		num[i++] = buffer[cur++];
	}
	return atoi( num );
}

int bscanf( const char *fmt, ... ){

	int count = 0;
	va_list args;
	va_start( args, fmt );

	while( *fmt != '\0' ){
		while( *fmt == ' ' ) fmt++;

		if( *fmt != '%' ) return -1;
		fmt++;

		if( buffer[cur] == '\0' ) return count;

		switch( *fmt ){
			case 'c': {
				char ch = buffer[cur++];
				count++;
				*va_arg( args, char* ) = ch;
				break;
			}
			case 'd': {
				int num = readInt();
				count++;
				*va_arg( args, int* ) = num;
				break;
			}
			default: {
				return -1;
			}
		}
		fmt++;
	}

	va_end( args );
	return count;
}
